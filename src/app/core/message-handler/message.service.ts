import { Injectable } from '@angular/core';
import { SocketClientService } from '../socket-client.service';
import { Observable } from 'rxjs/internal/Observable';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(private socketClient: SocketClientService) { }

  onMessage(): Observable<string> {
    return this.socketClient.onPlainMessage('/user/2/topic/mock/reply');
  }
}
