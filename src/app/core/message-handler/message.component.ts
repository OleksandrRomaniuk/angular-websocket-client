import { Component, OnDestroy, OnInit } from '@angular/core';
import { MessageService } from './message.service';
import { Subject } from 'rxjs/internal/Subject';
import { takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-message-handler',
  templateUrl: './message-handler.component.html',
  styleUrls: ['./message-handler.component.css']
})
export class MessageComponent implements OnInit, OnDestroy {

  messages: string[];
  private unsubscribeSubject: Subject<void> = new Subject<void>();

  constructor(private messageService: MessageService) {
    this.messages = [];
  }

  ngOnInit(): void {
    this.messageService.onMessage().pipe(takeUntil(this.unsubscribeSubject)).subscribe(message => this.messages.push(message));
  }

  ngOnDestroy(): void {
    this.unsubscribeSubject.next();
    this.unsubscribeSubject.complete();
  }

  clear(event: Event) {
    this.messages = [];
    event.preventDefault();
  }
}
